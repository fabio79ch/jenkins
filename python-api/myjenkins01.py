#!/usr/bin/env python
# coding: utf-8

import jenkins
import sys
import json

token     = '69cb9863fb0767916827ae8784c20487'
username  = 'admin'


ci_jenkins_url = "http://localhost:8888"

job = 'example/example04'

j = jenkins.Jenkins(url=ci_jenkins_url, username=username, password=token)


###############################
if __name__ == "__main__":

      # Incredibly to make get_version work you're forced to switch ON the 'Overall Read' security setting in Jenkins
      # https://stackoverflow.com/questions/40253108/get-jenkins-badhttpexception-when-try-to-get-version-info-from-jenkins-run-on-do/44346465#44346465 
      version = j.get_version()

      user    = j.get_whoami()
      """
      In [2]: j.get_whoami()
      Out[2]: 
      {u'_class': u'hudson.model.User',
       u'absoluteUrl': u'http://localhost:8888/user/admin',
       u'description': None,
       u'fullName': u'Admin',
       u'id': u'admin',
       u'property': [{u'_class': u'jenkins.security.ApiTokenProperty'},
        {u'_class': u'io.jenkins.blueocean.autofavorite.user.FavoritingUserProperty'},
        {u'_class': u'com.cloudbees.plugins.credentials.UserCredentialsProvider$UserCredentialsProperty'},
        {u'_class': u'hudson.plugins.emailext.watching.EmailExtWatchAction$UserProperty',
         u'triggers': []},
        {u'_class': u'hudson.plugins.favorite.user.FavoriteUserProperty'},
        {u'_class': u'hudson.model.MyViewsProperty'},
        {u'_class': u'org.jenkinsci.plugins.displayurlapi.user.PreferredProviderUserProperty'},
        {u'_class': u'hudson.model.PaneStatusProperties'},
        {u'_class': u'hudson.search.UserSearchProperty', u'insensitiveSearch': True},
        {u'_class': u'hudson.security.HudsonPrivateSecurityRealm$Details'},
        {u'_class': u'hudson.tasks.Mailer$UserProperty',
         u'address': u'admin@admin.com'},
        {u'_class': u'jenkins.security.LastGrantedAuthoritiesProperty'}]}
      """   

      print('Hello %s from Jenkins %s' % (user['fullName'], version))

      # works, but it's pointless here
      #plugins = j.get_plugins_info()
      #print plugins

      jobs = j.get_jobs()
      print jobs

      print json.dumps ( j.get_job_info( job ), indent=2, separators=(',', ': ') ) 
     
      """
      In [3]: jenkins.Jenkins.build_job?
      Signature: jenkins.Jenkins.build_job(self, name, parameters=None, token=None)
      Docstring:
      Trigger build job.
      
      This method returns a queue item number that you can pass to
      :meth:`Jenkins.get_queue_item`. Note that this queue number is only
      valid for about five minutes after the job completes, so you should
      get/poll the queue information as soon as possible to determine the
      job's URL.
      
      :param name: name of job
      :param parameters: parameters for job, or ``None``, ``dict``
      :param token: Jenkins API token
      :returns: ``int`` queue item
      File:      /usr/local/lib/python2.7/site-packages/jenkins/__init__.py
      Type:      instancemethod
      """
      j.build_job( job )
      #j.build_job( name=job, parameters=None, token=token )
      print( j.get_queue_info() )

