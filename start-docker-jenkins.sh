docker run -u root  -d -p 8888:8080 -p 22222:22222 -v jenkins-data:/var/jenkins_home \
  -v /var/run/docker.sock:/var/run/docker.sock \
-e JAVA_OPTS="-Dorg.jenkinsci.plugins.durabletask.BourneShellScript.HEARTBEAT_CHECK_INTERVAL=300"  \
--name jenkins \
jenkinsci/blueocean 
